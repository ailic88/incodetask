package com.task

import android.arch.lifecycle.ViewModel
import android.content.Context
import android.content.Intent
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

class ImageDetailsActivityViewModel : ViewModel() {
    var image: Image? = null

    fun setupUI(imageView: ImageView, description: TextView) {
        image?.let {
            Picasso.get().load(it.photoUrl).into(imageView)
            description.text = getDescriptionText()
        }
    }

    private fun getDescriptionText(): String? {
        return image?.let { "${it.title}\n ${it.publishedDate}" }
    }

    fun shareImageUrl(context: Context) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/*"
        intent.putExtra(Intent.EXTRA_TEXT, image?.photoUrl)
        context.startActivity(Intent.createChooser(intent, context.getString(R.string.share_image_link_message)))

    }
}