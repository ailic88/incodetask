package com.task

import retrofit2.Call
import retrofit2.http.GET

interface ImageService {
    companion object {
        const val BASE_URL = "http://www.json-generator.com"
    }

    @GET("/api/json/get/cftPFNNHsi")
    fun loadImages(): Call<ArrayList<Image>>
}