package com.task

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.widget.ImageView
import android.widget.TextView

class ImageDetailsActivity : AppCompatActivity() {

    companion object {
        const val IMAGE_EXTRA = "IMAGE_EXTRA"
    }

    private lateinit var previewImageView: ImageView
    private lateinit var descriptionTextView: TextView
    private lateinit var viewModel: ImageDetailsActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_details)
        viewModel = ViewModelProviders.of(this).get(ImageDetailsActivityViewModel::class.java)
        previewImageView = findViewById(R.id.previewImageView)
        descriptionTextView = findViewById(R.id.imageDescriptionTextView)
        setupUi()

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.image_details_menu, menu)
        val item = menu.findItem(R.id.shareMenuItem)
        item.setOnMenuItemClickListener {
            viewModel.shareImageUrl(this)
            return@setOnMenuItemClickListener true
        }
        return true
    }

    private fun setupUi() {
        if (viewModel.image == null) {
            intent?.let {
                val image = it.getParcelableExtra(IMAGE_EXTRA) as Image
                viewModel.image = image
            }
        }
        viewModel.setupUI(previewImageView, descriptionTextView)

    }

}
