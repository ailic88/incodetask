package com.task

import android.Manifest
import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu


class MainActivity : AppCompatActivity() {

    companion object {
        private const val PERMISSION_CODE = 1
        private const val CAMERA_REQUEST = 100
        private const val DATA_EXTRA = "data"
    }

    private val permissions = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    private lateinit var viewModel: MainActivityViewModel
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: ImageAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recyclerView = findViewById(R.id.imagesRecyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = ImageAdapter()
        recyclerView.adapter = adapter
        viewModel = ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
        viewModel.getImagesLiveData().observe(this, Observer { list ->
            list?.let { adapter.setData(it) }
        })
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        val item = menu.findItem(R.id.cameraMenuItem)
        item.setOnMenuItemClickListener {
            checkPermissionsAndStartTakePhotoActivity()
            true
        }
        return true
    }

    private fun checkPermissionsAndStartTakePhotoActivity() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) +
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(this, permissions, PERMISSION_CODE)
        } else {
            startTakePhotoActivity()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                startTakePhotoActivity()
            }
        }
    }

    private fun startTakePhotoActivity() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(cameraIntent, CAMERA_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            data?.let {
                it.extras?.let { extra ->
                    val photo = extra.get(DATA_EXTRA) as Bitmap
                    viewModel.addNewImage(applicationContext, photo)
                }
            }
        }
    }
}
