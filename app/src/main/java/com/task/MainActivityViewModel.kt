package com.task

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.graphics.Bitmap
import android.provider.MediaStore
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*


class MainActivityViewModel : ViewModel() {

    private lateinit var imageListLiveData: MutableLiveData<ArrayList<Image>>
    private val imageService: ImageService

    init {
        val gson = GsonBuilder()
            .setLenient()
            .create()
        val retrofit = Retrofit.Builder()
            .baseUrl(ImageService.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        imageService = retrofit.create(ImageService::class.java)
    }

    fun getImagesLiveData(): LiveData<ArrayList<Image>> {
        //prevent loading twice on rotation
        if (!::imageListLiveData.isInitialized) {
            imageListLiveData = MutableLiveData()
            loadImages()
        }
        return imageListLiveData
    }

    private fun loadImages() {
        imageService.loadImages().enqueue(object : Callback<ArrayList<Image>> {
            override fun onResponse(call: Call<ArrayList<Image>>, response: Response<ArrayList<Image>>) {
                if (response.isSuccessful) {
                    imageListLiveData.postValue(response.body())
                }
            }

            override fun onFailure(call: Call<ArrayList<Image>>, t: Throwable) {
                //do nothing
            }
        })
    }

    fun addNewImage(applicationContext: Context, photo: Bitmap) {
        val newImageTitle = applicationContext.getString(R.string.new_image_title)
        val path = MediaStore.Images.Media.insertImage(applicationContext.contentResolver, photo, newImageTitle, null)
        val newImage = Image(title = newImageTitle, photoUrl = path, publishedDate = Date().toString())
        imageListLiveData.value?.let {
            it.add(0, newImage)
            imageListLiveData.value = it
        }

    }
}