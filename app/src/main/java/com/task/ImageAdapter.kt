package com.task

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.squareup.picasso.Picasso


class ImageAdapter : RecyclerView.Adapter<ImageViewHolder>() {

    var images: ArrayList<Image> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val inflatedView = LayoutInflater.from(parent.context)
            .inflate(R.layout.image_list_item, parent, false)
        return ImageViewHolder(inflatedView)
    }


    override fun getItemCount(): Int {
        return images.size
    }

    override fun onBindViewHolder(viewHolder: ImageViewHolder, position: Int) {
        val image = images.get(position)
        Picasso.get().load(image.photoUrl).into(viewHolder.imageView)
        viewHolder.imageView.setOnClickListener {
            startImageDetailsActivity(it.context, image)
        }
    }

    private fun startImageDetailsActivity(context: Context, image: Image) {
        val intent = Intent(context, ImageDetailsActivity::class.java)
        intent.putExtra(ImageDetailsActivity.IMAGE_EXTRA, image)
        context.startActivity(intent)
    }

    fun setData(images: ArrayList<Image>) {
        this.images = images
        notifyDataSetChanged()
    }

}

class ImageViewHolder(root: View) : RecyclerView.ViewHolder(root) {
    val imageView: ImageView = root.findViewById(R.id.itemImageView)

}
